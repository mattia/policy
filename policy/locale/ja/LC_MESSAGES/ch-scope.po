# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-10 16:09+0000\n"
"PO-Revision-Date: 2018-07-28 12:45+0800\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../ch-scope.rst:2
msgid "About this manual"
msgstr "このマニュアルについて"

#: ../../ch-scope.rst:7
msgid "Scope"
msgstr "扱う範囲"

#: ../../ch-scope.rst:9
msgid ""
"This manual describes the policy requirements for the Debian "
"distribution. This includes the structure and contents of the Debian "
"archive and several design issues of the operating system, as well as "
"technical requirements that each package must satisfy to be included in "
"the distribution."
msgstr ""
"このマニュアルでは、Debian ディストリビューションのポリシー (方針)、すなわち Debian "
"に要求されるいくつかの必要条件について説明します。このポリシーには、Debian "
"アーカイブの構成と内容、オペレーティングシステムとしての Debian "
"の設計に関するいくつかの事項に加えて、それぞれのパッケージがディストリビューション"
"に受け入れられるために満たさなければならない技術的な必要条件も含まれます。"

#: ../../ch-scope.rst:15
msgid ""
"This manual also describes Debian policy as it relates to creating Debian"
" packages. It is not a tutorial on how to build packages, nor is it "
"exhaustive where it comes to describing the behavior of the packaging "
"system. Instead, this manual attempts to define the interface to the "
"package management system with which the developers must be conversant.  "
"[#]_"
msgstr ""
"このマニュアルは Debian パッケージ作成に沿った形で Debian ポリシーを記載していますが、"
"これはパッケージ作成手順を記載した手引きではなく、パッケージングシステムの挙動について"
"網羅的に記載したものでもありません。このマニュアルは、そのような手引きというよりは、"
"開発者が精通しておく必要のあるパッケージ管理システムとのインターフェースを規定すること"
"をねらったものです。"
"[#]_"

#: ../../ch-scope.rst:22
msgid ""
"The footnotes present in this manual are merely informative, and are not "
"part of Debian policy itself."
msgstr ""
"このマニュアル中の脚注は単に説明のためのものであり、Debian ポリシーの一部ではありません。"

#: ../../ch-scope.rst:25
msgid ""
"The appendices to this manual are not necessarily normative, either. "
"Please see :doc:`ap-pkg-scope` for more information."
msgstr ""
"このマニュアルの付録部分は必ずしも規範に沿ってはいません。詳細は :doc:`ap-pkg-scope` を参照ください。"

#: ../../ch-scope.rst:28
msgid ""
"In the normative part of this manual, the words *must*, *should* and "
"*may*, and the adjectives *required*, *recommended* and *optional*, are "
"used to distinguish the significance of the various guidelines in this "
"policy document. Packages that do not conform to the guidelines denoted "
"by *must* (or *required*) will generally not be considered acceptable for"
" the Debian distribution. Non-conformance with guidelines denoted by "
"*should* (or *recommended*) will generally be considered a bug, but will "
"not necessarily render a package unsuitable for distribution. Guidelines "
"denoted by *may* (or *optional*) are truly optional and adherence is left"
" to the maintainer's discretion."
msgstr ""
"このマニュアルの規範に従ってかかれている部分では *しなければならない*、"
"*すべきである*、*してもよい* という語、および *必要な*、*推奨された* 、*オプションとして* "
"という形容詞はこのポリシー文書中の様々なガイドラインの重要度を示すために使われています。"
"*しなければならない* とされた、または *必要な* とされたガイドラインに従っていない"
"パッケージは一般的に Debian ディストリビューションに受け入れて配布することはできないと"
"判断されます。一方、*すべきである* または *推奨された* ガイドラインへの不適合は通常は"
"バグと見なされますが、ディストリビューションとして配布不適合とまではされません。"
"*してもよい* または *オプションとして* となっているガイドラインは本当にオプションであり、"
"ガイドラインに従うかどうかはメンテナの判断に任されています。"

#: ../../ch-scope.rst:39
msgid ""
"These classifications are roughly equivalent to the bug severities "
"*serious* (for *must* or *required* directive violations), *minor*, "
"*normal* or *important* (for *should* or *recommended* directive "
"violations) and *wishlist* (for *optional* items).  [#]_"
msgstr ""
"この分類は大雑把に言って、バグ分類の *serious* (*しなければならない* や *必要な* の部分に違反)、"
"*minor*、*normal* と *important* (*すべきである* または *推奨された* の部分に違反)、*wishlist*"
"(*オプションとして* の類) に相当しています。[#]_"

#: ../../ch-scope.rst:44
msgid ""
"Much of the information presented in this manual will be useful even when"
" building a package which is to be distributed in some other way or is "
"intended for local use only."
msgstr ""
"このマニュアルに載っている情報の多くは、Debian アーカイブへの収録以外の方法で配布するような、あるいはお手元で使うだけで配布はしないといったパッケージを作成する際にも役に立つでしょう。"
#: ../../ch-scope.rst:48
msgid ""
"udebs (stripped-down binary packages used by the Debian Installer) do not"
" comply with all of the requirements discussed here. See the `Debian "
"Installer internals manual "
"<https://d-i.debian.org/doc/internals/ch03.html>`_ for more "
"information about them."
msgstr ""
"udebs (Debian インストーラで用いる機能限定版のバイナリパッケージ) "
"は、以下で説明されている要求事項に必ずしも従っていません。詳細は"
"`Debian Installer internals manual <https://d-i.debian.org/doc/internals/ch03.html>`_ "
"を参照してください。"

#: ../../ch-scope.rst:57
msgid "New versions of this document"
msgstr "この文書の新しい版"

#: ../../ch-scope.rst:59
msgid ""
"This manual is distributed via the Debian package `debian-policy "
"<https://packages.debian.org/debian-policy>`_."
msgstr ""

#: ../../ch-scope.rst:62
msgid ""
"The current version of this document is also available from the Debian "
"web mirrors at https://www.debian.org/doc/debian-policy/. Also available "
"from the same directory are several other formats: `policy.epub "
"<https://www.debian.org/doc/debian-policy/policy.epub>`_, `policy.txt "
"<https://www.debian.org/doc/debian-policy/policy.txt>`_ and `policy.pdf "
"<https://www.debian.org/doc/debian-policy/policy.pdf>`_. Included in both"
" the same directory and in the debian-policy package is a standalone copy"
" of :doc:`upgrading-checklist`, which indicates policy changes between "
"versions of this document."
msgstr ""

#: ../../ch-scope.rst:78
msgid "Authors and Maintainers"
msgstr "作者とメンテナ"

#: ../../ch-scope.rst:81
msgid "Early history"
msgstr ""

#: ../../ch-scope.rst:83
msgid ""
"Originally called \"Debian GNU/Linux Policy Manual\", this manual was "
"initially written in 1996 by Ian Jackson. It was revised on November "
"27th, 1996 by David A. Morris. Christian Schwarz added new sections on "
"March 15th, 1997, and reworked/restructured it in April-July 1997. "
"Christoph Lameter contributed the \"Web Standard\". Julian Gilbey largely"
" restructured it in 2001.  Since September 1998, changes to the contents "
"of this document have been co-ordinated by means of the `debian-policy "
"mailing list <mailto:debian-policy@lists.debian.org>`_"
msgstr ""
"この文書は、最初は \"Debian GNU/Linux Policy Manual\" という名で1996年に Ian Jackson さんにより書かれました。最初の改訂は1996年11月27日に David A. Morris さんによるもので、Christian Schwarz さんが1997年3月15日に新たな章を加え、1997年の 4 月から 7 月にかけて、手を入れてまとめ直しました。Christoph Lameter さんが \"Web Standard\" の部分を作成しました。Julian Gilbey さんが 2001 年に大幅な整理を行ないました。1998年9月以降は、この文書の内容は `debian-policy メーリングリスト <mailto:debian-policy@lists.debian.org>`_ での共同作業に移りました。"

#: ../../ch-scope.rst:93
msgid "Current process"
msgstr "現在のプロセス"

#: ../../ch-scope.rst:95
msgid ""
"The Policy Editors are DPL delegates with responsibility for the contents"
" of this document (see the Debian Constitution for the meaning of \"DPL "
"delegate\").  However, the Policy Editors further delegate their "
"editorial power to a process of establishing project member consensus on "
"the debian-policy mailing list, as described in :doc:`ap-process`.  The "
"current Policy Editors are:"
msgstr ""

#: ../../ch-scope.rst:102
msgid "Russ Allbery"
msgstr "Russ Allbery"

#: ../../ch-scope.rst:104
msgid "Bill Allombert"
msgstr "Bill Allombert"

#: ../../ch-scope.rst:106
msgid "Andreas Barth"
msgstr "Andreas Barth"

#: ../../ch-scope.rst:108
msgid "Sean Whitton"
msgstr "Sean Whitton"

#: ../../ch-scope.rst:111
msgid "Improvements"
msgstr ""

#: ../../ch-scope.rst:113
msgid ""
"While the authors of this document have tried hard to avoid typos and "
"other errors, these do still occur. If you discover an error in this "
"manual or if you want to give any comments, suggestions, or criticisms "
"please send an email to the Debian Policy Mailing List, debian-"
"policy@lists.debian.org, or submit a bug report against the ``debian-"
"policy`` package."
msgstr ""
"この文書の筆者たちは、誤植その他のいかなる誤りも入れないようできる限り努力はしましたが、それでも間違いは生じます。もしこの文書の内容に誤りを見つけたり、何らかの意見や提案、批評を私たちに伝えたいという場合には、Debian ポリシーメーリングリスト debian-policy@lists.debian.org まで電子メールをお送り頂くか、``debian-policy``  パッケージに対してバグ報告を出して頂けると幸いです。"

#: ../../ch-scope.rst:120
msgid ""
"Please do not try to reach the individual authors or maintainers of the "
"Policy Manual regarding changes to the Policy."
msgstr ""
"ポリシーの変更に関しては、直接著者やポリシーマニュアルのメンテナに連絡しないでください。"

#: ../../ch-scope.rst:123
msgid ""
"New techniques and functionality are generally implemented in the Debian "
"archive (long) before they are detailed in this document.  This is not "
"considered to be a problem: there is a consensus in the Debian Project "
"that the task of keeping this document up-to-date should never block "
"making improvements to Debian.  Nevertheless, it is better to submit "
"patches to this document sooner rather than later.  This reduces the "
"amount of work that is needed on the part of others to get themselves up-"
"to-speed on new best practices."
msgstr ""

#: ../../ch-scope.rst:135
msgid "Related documents"
msgstr "関連文書"

#: ../../ch-scope.rst:137
msgid ""
"There are several other documents other than this Policy Manual that are "
"necessary to fully understand some Debian policies and procedures."
msgstr ""

#: ../../ch-scope.rst:140
msgid "The external \"sub-policy\" documents are referred to in:"
msgstr ""

#: ../../ch-scope.rst:142
msgid ":ref:`s-fhs`"
msgstr ":ref:`s-fhs`"

#: ../../ch-scope.rst:144
msgid ":ref:`s-virtual-pkg`"
msgstr ":ref:`s-virtual-pkg`"

#: ../../ch-scope.rst:146
msgid ":ref:`s-menus`"
msgstr ":ref:`s-menus`"

#: ../../ch-scope.rst:148
msgid ":ref:`s-perl`"
msgstr ":ref:`s-perl`"

#: ../../ch-scope.rst:150
msgid ":ref:`s-maintscriptprompt`"
msgstr ":ref:`s-maintscriptprompt`"

#: ../../ch-scope.rst:152
msgid ":ref:`s-emacs`"
msgstr ":ref:`s-emacs`"

#: ../../ch-scope.rst:154
msgid ""
"In addition to those, which carry the weight of policy, there is the "
"Debian Developer's Reference. This document describes procedures and "
"resources for Debian developers, but it is *not* normative; rather, it "
"includes things that don't belong in the Policy, such as best practices "
"for developers."
msgstr ""
"これらポリシーの一部を担う文書とは別に、Debian 開発者リファレンスがあります。こちらの方の文書は Debian の開発者の手順とリソースについて記載されたものですが、規範となるものでは*ありません*。むしろポリシーに含まれない内容、例えば好ましい開発者の作業例、などを記載したものです。"

#: ../../ch-scope.rst:160
msgid ""
"The Developer's Reference is available in the developers-reference "
"package. It's also available from the Debian web mirrors at "
"https://www.debian.org/doc/developers-reference/."
msgstr ""
"開発者リファレンスは developers-reference パッケージに収録されています（訳注：日本語訳は developers-reference-ja です）。また、Debian web ミラーの https://www.debian.org/doc/developers-reference/ にあります。"

#: ../../ch-scope.rst:164
msgid ""
"Finally, a :ref:`specification for machine-readable copyright files "
"<s-copyrightformat>` is maintained as part of the debian-policy package "
"using the same procedure as the other policy documents. Use of this "
"format is optional."
msgstr ""
"最後に、:ref:`specification for machine-readable copyright files <s-copyrightformat>` "
"(機械可読な形式の著作権宣言ファイル仕様) が、debian-policy パッケージの一部として、"
"他のポリシー文書と同様の手続きの元で維持管理されています。このフォーマットへの準拠は任意です。"

#: ../../ch-scope.rst:172
msgid "Definitions"
msgstr "用語"

#: ../../ch-scope.rst:174
msgid "The following terms are used in this Policy Manual:"
msgstr "以下の単語がこのポリシーマニュアルで使用されています:"

#: ../../ch-scope.rst:181
msgid "ASCII"
msgstr "ASCII"

#: ../../ch-scope.rst:177
msgid ""
"The character encoding specified by ANSI X3.4-1986 and its predecessor "
"standards, referred to in MIME as US-ASCII, and corresponding to an "
"encoding in eight bits per character of the first 128 `Unicode "
"<http://www.unicode.org/>`_ characters, with the eighth bit always zero."
msgstr ""
"ANSI X3.4-1986 およびその先行する標準で規定された文字エンコーディングであり、"
"MIME では US-ASCII として参照され、一文字を 8 ビット (列) で表現するエンコーディング"
"である。 `Unicode <http://www.unicode.org/>`_ の最初の 128 文字 (8 ビット目は常に 0) に一致します。"

#: ../../ch-scope.rst:188
msgid "UTF-8"
msgstr "UTF-8"

#: ../../ch-scope.rst:184
msgid ""
"The transformation format (sometimes called encoding) of `Unicode "
"<http://www.unicode.org/>`_ defined by `RFC 3629 <https://www.rfc-"
"editor.org/rfc/rfc3629.txt>`__. UTF-8 has the useful property of having "
"ASCII as a subset, so any text encoded in ASCII is trivially also valid "
"UTF-8."
msgstr ""
"`Unicode <http://www.unicode.org/>`_ の転送形式 (エンコーディングと呼ばれることもあります) で、`RFC 3629 <https://www.rfc-editor.org/rfc/rfc3629.txt>`__ で定義されています。UTF-8 は ASCII をサブセットとして含むという役に立つ性質を持っているため、ASCII エンコードされたテキストは自動的に有効な UTF-8 テキストにもなります。"

#: ../../ch-scope.rst:191
msgid ""
"Informally, the criteria used for inclusion is that the material meet one"
" of the following requirements:"
msgstr ""
"非公式には、ここに収録される内容の選択基準は、次のどちらかに属していることです:"

#: ../../ch-scope.rst:201
msgid "Standard interfaces"
msgstr "標準インターフェース"

#: ../../ch-scope.rst:195
msgid ""
"The material presented represents an interface to the packaging system "
"that is mandated for use, and is used by, a significant number of "
"packages, and therefore should not be changed without peer review. "
"Package maintainers can then rely on this interface not changing, and the"
" package management software authors need to ensure compatibility with "
"this interface definition. (Control file and changelog file formats are "
"examples.)"
msgstr ""
"記載された内容がパッケージングシステムのインターフェースとして使うべきものであり、実際に多数のパッケージで使われており、従って綿密なレビューなしには変更するべきでないようなものである場合です。そのような場合にはパッケージメンテナはインターフェースがころころ変わらないことを期待できますし、パッケージ管理ソフトウェアの作者はそのインターフェース定義への互換性を確認するだけですみます(コントロールファイルや changelog ファイル形式がその一例です)。"

#: ../../ch-scope.rst:206
msgid "Chosen Convention"
msgstr "選ばれた取り決め"

#: ../../ch-scope.rst:204
msgid ""
"If there are a number of technically viable choices that can be made, but"
" one needs to select one of these options for inter-operability. The "
"version number format is one example."
msgstr ""
"技術的には取り得る選択肢はいくつかありますが、相互互換性のためそのうちの一つ"
"を選ばなければならない場合です。バージョン番号の形式などがその例です。"

#: ../../ch-scope.rst:208
msgid ""
"Please note that these are not mutually exclusive; selected conventions "
"often become parts of standard interfaces."
msgstr ""
"これらが相互に排他なものではないことに注意してください。選ばれた取り決めが"
"標準インターフェースになることはしばしばあります。"

#: ../../ch-scope.rst:212
msgid ""
"Compare RFC 2119. Note, however, that these words are used in a different"
" way in this document."
msgstr ""
"RFC 2119 参照。但し、これらの単語は本文中で違ったやり方で使われています。"
